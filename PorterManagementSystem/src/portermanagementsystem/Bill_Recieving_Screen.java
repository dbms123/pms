
package portermanagementsystem;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JFrame;

public class Bill_Recieving_Screen extends javax.swing.JFrame {
    int userid;Timer timer=new Timer();int rid;int mintCount=0;
    public Bill_Recieving_Screen(int userid,int rid) {
        initComponents();
        setid(userid);
        setrid(rid);
        name.setText(Request.getCID(rid)+"");
        weight.setText(Request.getweight(rid)+"");
        total.setText(Request.getCost(rid)+"");
        status.setText(Bill.getstatus(rid));
        start();
        name1.setText(SystemUser.userName(userid));
        error.setText("");
    }
int arrivedCompleteSwitch=0;    
void setrid(int rid)
{
this.rid=rid;
}
int getrid()
{
return rid;
}
int getid()
  {
   return userid;
  }
  void setid(int userid)
  {
  this.userid=userid;
  }
   public String time()
        { DateFormat df = new SimpleDateFormat("HH:mm:ss");
       Date dateobj = new Date();
      return df.format(dateobj);}
public String getDate()
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	Date date = new Date();
	return dateFormat.format(date);
    }
    
  TimerTask task=new TimerTask()
  {
  public void run()
  {
  time.setText(time());
  date.setText(getDate());
  checkStatus();

  
  }
  
  };

  public void  start()
  {
  
  timer.scheduleAtFixedRate(task, 1100, 1100);
  
  }
  public void checkStatus()
  {String s=Bill.getstatus(rid);
  int r=Bill.getRecievedAmount(rid);
  PaidAmount.setText(r+"");
  System.out.println("paidAmount="+r);
  System.out.println("status"+status.getText());
  status.setText(s);
  }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        time = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        name1 = new javax.swing.JLabel();
        back = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        date = new javax.swing.JLabel();
        jButton4 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        employeename1 = new javax.swing.JLabel();
        name = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        weight = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        done = new javax.swing.JButton();
        jLabel22 = new javax.swing.JLabel();
        total = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        status = new javax.swing.JLabel();
        PayBill2 = new javax.swing.JLabel();
        star = new javax.swing.JComboBox<>();
        error = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        PaidAmount = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(40, 50, 80));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        time.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        time.setForeground(new java.awt.Color(254, 192, 0));
        time.setText("Time");
        jPanel1.add(time, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 490, 100, 30));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/portermanagementsystem/porterlogo.jpg"))); // NOI18N
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 160, 310, 220));

        jLabel2.setIcon(new javax.swing.ImageIcon("C:\\Users\\zaink\\Desktop\\p.png")); // NOI18N
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 20, 90, 90));

        name1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        name1.setForeground(new java.awt.Color(254, 192, 0));
        name1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        name1.setText("hghkhh");
        jPanel1.add(name1, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 120, 130, 30));

        back.setBackground(new java.awt.Color(255, 255, 255));
        back.setIcon(new javax.swing.ImageIcon("C:\\Users\\zaink\\Desktop\\backbutton2.png")); // NOI18N
        back.setBorder(null);
        back.setBorderPainted(false);
        back.setContentAreaFilled(false);
        back.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        back.setDefaultCapable(false);
        back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backActionPerformed(evt);
            }
        });
        jPanel1.add(back, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 60, 50, 50));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 360, 530));

        jPanel2.setBackground(new java.awt.Color(0, 102, 110));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        date.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        date.setForeground(new java.awt.Color(254, 192, 0));
        date.setText("Date");
        jPanel2.add(date, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 490, 80, 30));

        jButton4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton4.setForeground(new java.awt.Color(204, 102, 0));
        jButton4.setText("X");
        jButton4.setBorder(null);
        jButton4.setBorderPainted(false);
        jButton4.setContentAreaFilled(false);
        jButton4.setFocusPainted(false);
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton4, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 0, 30, 30));

        jButton6.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButton6.setForeground(new java.awt.Color(204, 102, 0));
        jButton6.setText("-");
        jButton6.setBorder(null);
        jButton6.setBorderPainted(false);
        jButton6.setContentAreaFilled(false);
        jButton6.setFocusPainted(false);
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton6, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 0, 30, 30));

        employeename1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        employeename1.setForeground(new java.awt.Color(254, 192, 0));
        employeename1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        employeename1.setText(" Bill ");
        jPanel2.add(employeename1, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 70, 170, 30));

        name.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        name.setForeground(new java.awt.Color(254, 192, 0));
        name.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        name.setText("Cost");
        name.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(name, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 130, 90, 30));

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(254, 192, 0));
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setText("Name");
        jLabel13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 130, 90, 30));

        weight.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        weight.setForeground(new java.awt.Color(254, 192, 0));
        weight.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        weight.setText("Cost");
        weight.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(weight, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 170, 90, 30));

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(254, 192, 0));
        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel19.setText("Weight");
        jLabel19.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 170, 90, 30));

        done.setText("Done!");
        done.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        done.setBorderPainted(false);
        done.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                doneActionPerformed(evt);
            }
        });
        jPanel2.add(done, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 410, 190, 40));

        jLabel22.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel22.setForeground(new java.awt.Color(254, 192, 0));
        jLabel22.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel22.setText("Total");
        jLabel22.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(jLabel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 210, 90, 30));

        total.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        total.setForeground(new java.awt.Color(254, 192, 0));
        total.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        total.setText("Cost");
        total.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(total, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 210, 90, 30));

        jLabel23.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(254, 192, 0));
        jLabel23.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel23.setText("Status");
        jLabel23.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(jLabel23, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 250, 90, 30));

        status.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        status.setForeground(new java.awt.Color(254, 192, 0));
        status.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        status.setText("Cost");
        status.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(status, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 250, 90, 30));

        PayBill2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PayBill2.setForeground(new java.awt.Color(254, 192, 0));
        PayBill2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        PayBill2.setText("Rate Customer");
        PayBill2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(PayBill2, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 330, 190, 30));

        star.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Star", "1", "2", "3", "4", "5" }));
        star.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                starActionPerformed(evt);
            }
        });
        jPanel2.add(star, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 370, 190, 30));

        error.setForeground(new java.awt.Color(255, 0, 0));
        error.setText("jLabel3");
        jPanel2.add(error, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 460, 190, 20));

        jLabel24.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel24.setForeground(new java.awt.Color(254, 192, 0));
        jLabel24.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel24.setText("Paid Amount");
        jLabel24.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(jLabel24, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 290, 90, 30));

        PaidAmount.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PaidAmount.setForeground(new java.awt.Color(254, 192, 0));
        PaidAmount.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        PaidAmount.setText("Cost");
        PaidAmount.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(PaidAmount, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 290, 90, 30));

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 0, 390, 530));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        System.exit(0);       
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        this.setState(JFrame.ICONIFIED);

       
    }//GEN-LAST:event_jButton6ActionPerformed

    private void backActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backActionPerformed

this.dispose();
new Porter_Screen(userid).setVisible(true);
    
    }//GEN-LAST:event_backActionPerformed

    private void doneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_doneActionPerformed
error.setText("");int count=0;
if(star.getSelectedItem().toString().equals("select star"))
{ error.setText("Select Star");count++;}
if(PaidAmount.getText().equals(""))
{count++; error.setText("Bill has not been paid yet");}
if(count==0)
{
int a=Integer.parseInt(PaidAmount.getText());
Wallet.createWallet(userid,a);
this.dispose();
new Porter_Screen(userid).setVisible(true);
}
    }//GEN-LAST:event_doneActionPerformed

    private void starActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_starActionPerformed
        
    }//GEN-LAST:event_starActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Bill_Recieving_Screen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Bill_Recieving_Screen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Bill_Recieving_Screen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Bill_Recieving_Screen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel PaidAmount;
    private javax.swing.JLabel PayBill2;
    private javax.swing.JButton back;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel date;
    private javax.swing.JButton done;
    private javax.swing.JLabel employeename1;
    private javax.swing.JLabel error;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel name;
    private javax.swing.JLabel name1;
    private javax.swing.JComboBox<String> star;
    private javax.swing.JLabel status;
    private javax.swing.JLabel time;
    private javax.swing.JLabel total;
    private javax.swing.JLabel weight;
    // End of variables declaration//GEN-END:variables
}
