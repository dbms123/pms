package portermanagementsystem;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBConnect2 
{
    public static Statement s;
    public static Connection c;
    public static void getConnection()
    {
    
    
    try
    {

     System.out.println("\"---------------\\nRequesting Connection... \"");
     Class.forName("oracle.jdbc.driver.OracleDriver");
     c=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl","controller","27379");
     s=c.createStatement();
     System.out.println("Connection Established...\n--------------- ");
    }
    catch(ClassNotFoundException | SQLException e)
    {
    System.out.println(e);
    }
    }
    public static Statement getStatement()
    {
    return s;
    }
    public static void disconnect()
    {
    try
    {
    c.close();
    System.out.println("---------Diconnecting---------------");

    }
    catch(SQLException e)
    {
    System.out.println(e);
    }
    }
}
