
package portermanagementsystem;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author zaink
 */
public class CustomerRating 
{
private static int RID;
private static int CID;
private static int PID;
private static int Rating;

public static void rateCustomer(int rid,int pid,int cid,int rating)
{

//DBConnect.getConnection();
    try {
        DBConnect.getStatement().executeUpdate("insert into customerrating values("+rating+","+pid+","+cid+","+rid+")");
    } catch (SQLException ex) {
        Logger.getLogger(PorterRating.class.getName()).log(Level.SEVERE, null, ex);
    }
//DBConnect.disconnect();
}
public static int getRating(int cid)
{int a=0;
//DBConnect.getConnection();
    try {
        ResultSet r=DBConnect.getStatement().executeQuery("select rating from customerrating where cid="+cid);
        while(r.next())
        {     a+=r.getInt("rating");}
    } catch (SQLException ex) {
        Logger.getLogger(PorterRating.class.getName()).log(Level.SEVERE, null, ex);
    }
//DBConnect.disconnect();
return a;
}    
}
