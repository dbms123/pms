
package portermanagementsystem;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JFrame;

public class Bill_Screen extends javax.swing.JFrame {
    int userid;Timer timer=new Timer();int rid;int mintCount=0;
    public Bill_Screen(int userid,int rid) {
        
        initComponents();
        setid(userid);
        name.setText(SystemUser.userName(userid));
        start();
        setrid(rid);
        name1.setText(SystemUser.userName(Request.getPID(rid)));
        total.setText(""+Request.getCost(rid));
        back.setVisible(false);
        billerror.setText("");
        starerror.setText("");
        Bill.generateBill(rid,Integer.parseInt(total.getText()));
        
    }
int arrivedCompleteSwitch=0;    
void setrid(int rid)
{
this.rid=rid;
}
int getrid()
{
return rid;
}
int getid()
  {
   return userid;
  }
  void setid(int userid)
  {
  this.userid=userid;
  }
   public String time()
        { DateFormat df = new SimpleDateFormat("HH:mm:ss");
       Date dateobj = new Date();
      return df.format(dateobj);}
public String getDate()
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	Date date = new Date();
	return dateFormat.format(date);
    }
    
  TimerTask task=new TimerTask()
  {
  public void run()
  {
  time.setText(time());
  date.setText(getDate());

 
  
  }
  
  };
  
 
  
  public void  start()
  {
  
  timer.scheduleAtFixedRate(task, 1000, 1000);
  
  }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        time = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        name1 = new javax.swing.JLabel();
        back = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        date = new javax.swing.JLabel();
        jButton4 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        employeename1 = new javax.swing.JLabel();
        name = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        PayBill1 = new javax.swing.JLabel();
        total = new javax.swing.JLabel();
        cancel = new javax.swing.JButton();
        jLabel15 = new javax.swing.JLabel();
        paybill = new javax.swing.JTextField();
        PayBill2 = new javax.swing.JLabel();
        PayBill3 = new javax.swing.JLabel();
        star = new javax.swing.JComboBox<>();
        billerror = new javax.swing.JLabel();
        starerror = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(40, 50, 80));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        time.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        time.setForeground(new java.awt.Color(254, 192, 0));
        time.setText("Time");
        jPanel1.add(time, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 490, 100, 30));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/portermanagementsystem/porterlogo.jpg"))); // NOI18N
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 160, 310, 220));

        jLabel2.setIcon(new javax.swing.ImageIcon("C:\\Users\\zaink\\Desktop\\p.png")); // NOI18N
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 20, 90, 90));

        name1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        name1.setForeground(new java.awt.Color(254, 192, 0));
        name1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        name1.setText("hghkhh");
        jPanel1.add(name1, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 120, 130, 30));

        back.setBackground(new java.awt.Color(255, 255, 255));
        back.setIcon(new javax.swing.ImageIcon("C:\\Users\\zaink\\Desktop\\backbutton2.png")); // NOI18N
        back.setBorder(null);
        back.setBorderPainted(false);
        back.setContentAreaFilled(false);
        back.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        back.setDefaultCapable(false);
        back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backActionPerformed(evt);
            }
        });
        jPanel1.add(back, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 60, 50, 50));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 360, 530));

        jPanel2.setBackground(new java.awt.Color(0, 102, 110));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        date.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        date.setForeground(new java.awt.Color(254, 192, 0));
        date.setText("Date");
        jPanel2.add(date, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 490, 80, 30));

        jButton4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton4.setForeground(new java.awt.Color(204, 102, 0));
        jButton4.setText("X");
        jButton4.setBorder(null);
        jButton4.setBorderPainted(false);
        jButton4.setContentAreaFilled(false);
        jButton4.setFocusPainted(false);
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton4, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 0, 30, 30));

        jButton6.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButton6.setForeground(new java.awt.Color(204, 102, 0));
        jButton6.setText("-");
        jButton6.setBorder(null);
        jButton6.setBorderPainted(false);
        jButton6.setContentAreaFilled(false);
        jButton6.setFocusPainted(false);
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton6, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 0, 30, 30));

        employeename1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        employeename1.setForeground(new java.awt.Color(254, 192, 0));
        employeename1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        employeename1.setText("Bill");
        jPanel2.add(employeename1, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 70, 170, 30));

        name.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        name.setForeground(new java.awt.Color(254, 192, 0));
        name.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        name.setText("Cost");
        name.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(name, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 130, 140, 30));

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(254, 192, 0));
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setText("Porter Name");
        jLabel13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 130, 130, 30));

        PayBill1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PayBill1.setForeground(new java.awt.Color(254, 192, 0));
        PayBill1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        PayBill1.setText("Pay Bill");
        PayBill1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(PayBill1, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 250, 280, 30));

        total.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        total.setForeground(new java.awt.Color(254, 192, 0));
        total.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        total.setText("Cost");
        total.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(total, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 170, 140, 30));

        cancel.setText("Submit");
        cancel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        cancel.setBorderPainted(false);
        cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelActionPerformed(evt);
            }
        });
        jPanel2.add(cancel, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 460, 190, 40));

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(254, 192, 0));
        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel15.setText("Total");
        jLabel15.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 170, 130, 30));

        paybill.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                paybillMouseClicked(evt);
            }
        });
        paybill.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                paybillActionPerformed(evt);
            }
        });
        paybill.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                paybillKeyReleased(evt);
            }
        });
        jPanel2.add(paybill, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 210, 140, 30));

        PayBill2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PayBill2.setForeground(new java.awt.Color(254, 192, 0));
        PayBill2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        PayBill2.setText("Rate US");
        PayBill2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(PayBill2, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 290, 130, 30));

        PayBill3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PayBill3.setForeground(new java.awt.Color(254, 192, 0));
        PayBill3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        PayBill3.setText("Pay Bill");
        PayBill3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(PayBill3, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 210, 130, 30));

        star.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Star", "1", "2", "3", "4", "5" }));
        jPanel2.add(star, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 290, 140, 30));

        billerror.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        billerror.setForeground(new java.awt.Color(255, 0, 51));
        billerror.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        billerror.setText("Invalid details");
        jPanel2.add(billerror, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 330, 140, 20));

        starerror.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        starerror.setForeground(new java.awt.Color(255, 0, 51));
        starerror.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        starerror.setText("Invalid details");
        jPanel2.add(starerror, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 330, 140, 20));

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 0, 390, 530));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        System.exit(0);        // TODO add your handling code here:
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        this.setState(JFrame.ICONIFIED);

       
    }//GEN-LAST:event_jButton6ActionPerformed

    private void backActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backActionPerformed

this.dispose();
new Request_screen_porter(userid).setVisible(true);
        // TODO add your handling code here:
    }//GEN-LAST:event_backActionPerformed

    private void cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelActionPerformed
int count=0;

if(paybill.getText().equals(""))
{
count++;
billerror.setText("Pay The Bill");
}
if(star.getSelectedItem().toString().equals("Select Star"))
{
count++;
starerror.setText("Rate us please");
}
if(count==0)
{
    int a=-Integer.parseInt(total.getText())+Integer.parseInt(paybill.getText());
    Wallet.createWallet(userid,a);
    Bill.payBill(rid,Integer.parseInt(paybill.getText()));
    PorterRating.ratePorter(rid,Request.getPID(rid),Request.getCID(rid),Integer.parseInt(star.getSelectedItem().toString()));
    this.dispose();
    new Customer_Screen(userid).setVisible(true);
}

        // TODO add your handling code here:
    }//GEN-LAST:event_cancelActionPerformed

    private void paybillActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_paybillActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_paybillActionPerformed

    private void paybillMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_paybillMouseClicked

billerror.setText("");
starerror.setText("");
        // TODO add your handling code here:
    }//GEN-LAST:event_paybillMouseClicked

    private void paybillKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_paybillKeyReleased
String s=paybill.getText();
s=s.toLowerCase();
s=s.replaceAll(" ", "");s=s.replaceAll("a", "");s=s.replaceAll("b", "");s=s.replaceAll("c", "");s=s.replaceAll("d", "");s=s.replaceAll("e", "");
s=s.replaceAll("f", "");s=s.replaceAll("g", "");s=s.replaceAll("h", "");s=s.replaceAll("i", "");s=s.replaceAll("j", "");s=s.replaceAll("k", "");
s=s.replaceAll("l", "");s=s.replaceAll("m", "");s=s.replaceAll("n", "");s=s.replaceAll("o", "");s=s.replaceAll("p", "");s=s.replaceAll("q", "");
s=s.replaceAll("r", "");s=s.replaceAll("s", "");s=s.replaceAll("t", "");s=s.replaceAll("u", "");s=s.replaceAll("v", "");s=s.replaceAll("w", "");
s=s.replaceAll("x", "");s=s.replaceAll("y", "");s=s.replaceAll("z", "");
s=s.replaceAll("`", "");s=s.replaceAll("~", "");s=s.replaceAll("!", "");s=s.replaceAll("#", "");s=s.replaceAll("@", "");s=s.replaceAll("$", "");
s=s.replaceAll("%", "");s=s.replaceAll("^", "");s=s.replaceAll("&", "");
s=s.replaceAll("-", "");s=s.replaceAll("_", "");s=s.replaceAll("=", "");s=s.replaceAll("/", "");
s=s.replaceAll("]", "");s=s.replaceAll("}", "");s=s.replaceAll(":", "");s=s.replaceAll(";", "");
s=s.replaceAll("'", "");s=s.replaceAll("|", "");s=s.replaceAll(",", "");s=s.replaceAll("<", "");s=s.replaceAll(">", "");
paybill.setText(s);
    }//GEN-LAST:event_paybillKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Bill_Screen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Bill_Screen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Bill_Screen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Bill_Screen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel PayBill1;
    private javax.swing.JLabel PayBill2;
    private javax.swing.JLabel PayBill3;
    private javax.swing.JButton back;
    private javax.swing.JLabel billerror;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton cancel;
    private javax.swing.JLabel date;
    private javax.swing.JLabel employeename1;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel name;
    private javax.swing.JLabel name1;
    private javax.swing.JTextField paybill;
    private javax.swing.JComboBox<String> star;
    private javax.swing.JLabel starerror;
    private javax.swing.JLabel time;
    private javax.swing.JLabel total;
    // End of variables declaration//GEN-END:variables
}
