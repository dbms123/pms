
package portermanagementsystem;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Wallet 
{
    private static int CID=0;
    private static int Credit=0;
    
    public static void refresh()
{
CID=0;
Credit=0;

}
public static boolean checkID(int cid)
{
//DBConnect.getConnection();
int a=0;
        try {
            ResultSet r=DBConnect.getStatement().executeQuery("select cid from wallet where cid="+cid);
            while(r.next())
            {
            a=r.getInt("cid");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Wallet.class.getName()).log(Level.SEVERE, null, ex);
        }
//DBConnect.disconnect();
        return a != 0;
}
public static void updateCredit(int cid,int credit)
{
    credit=credit+getCredit(cid);
 //DBConnect.getConnection();
        try {
           DBConnect.getStatement().executeUpdate("update wallet set credit ="+credit+"where cid="+cid);
        } catch (SQLException ex) {
            Logger.getLogger(Request.class.getName()).log(Level.SEVERE, null, ex);
        }
        //DBConnect.disconnect();
}
public static int getCredit(int cid)
{
//DBConnect.getConnection();
int a=0;
        try {
            ResultSet r=DBConnect.getStatement().executeQuery("select credit from wallet where cid="+cid);
            while(r.next())
            {
            a=r.getInt("credit");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Wallet.class.getName()).log(Level.SEVERE, null, ex);
        }
        //DBConnect.disconnect();
        return a;
}
public static void createWallet(int cid,int credit)
{
    
    if(checkID(cid))
    {
    updateCredit(cid,credit);
    }    
    else
    {
    //DBConnect.getConnection();
try {
            DBConnect.getStatement().executeQuery("Insert into wallet values("+cid+","+credit+")");
            
        } catch (SQLException ex) {
            Logger.getLogger(Wallet.class.getName()).log(Level.SEVERE, null, ex);
        }
//DBConnect.disconnect();
    }
}
}

