package portermanagementsystem;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author zaink
 */
public class Bill 
{
    private static int RID;
    private static int RecievedAmount;
    private static int TotalAmount;
    
    static void refresh()
    {
    RID=0;
    RecievedAmount=0;
    TotalAmount=0;
    }
    public static int getRecievedAmount(int rid)
    { int a=0;
    //DBConnect.getConnection();
        try {
            ResultSet r=DBConnect.getStatement().executeQuery("Select recievedamount from bill where rid="+rid);
            while(r.next())
            {
            a=r.getInt("recievedamount");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Bill.class.getName()).log(Level.SEVERE, null, ex);
        }
    // DBConnect.disconnect();
    return a;
    }
    public static int getTotalAmount(int rid)
    { int a=0;
   // DBConnect.getConnection();
        try {
            ResultSet r=DBConnect.getStatement().executeQuery("Select totalamount from bill where rid="+rid);
            while(r.next())
            {
            a=r.getInt("totalamount");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Bill.class.getName()).log(Level.SEVERE, null, ex);
        }
   //  DBConnect.disconnect();
    return a;
   
    }
    public static void generateBill(int rid,int totalamount)
    {
    
   // DBConnect.getConnection();
        try {
            DBConnect.getStatement().executeUpdate("insert into bill(rid,totalamount,status) values ("+rid+","+totalamount+",'Not Paid')");
        } catch (SQLException ex) {
            Logger.getLogger(Bill.class.getName()).log(Level.SEVERE, null, ex);
        }
   // DBConnect.disconnect();
    
    
    }
     public static String getstatus(int rid)
    { String a="";
   // DBConnect.getConnection();
        try {
            ResultSet r=DBConnect.getStatement().executeQuery("Select status from bill where rid="+rid);
            while(r.next())
            {
            a=r.getString("status");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Bill.class.getName()).log(Level.SEVERE, null, ex);
        }
    // DBConnect.disconnect();
    return a;
    }
    
    public static void payBill(int rid,int recievedAmount)
    {
    
   // DBConnect.getConnection();
        try {
            DBConnect.getStatement().executeUpdate("update bill set recievedamount ="+recievedAmount+",status='Paid' where rid="+rid);
        } catch (SQLException ex) {
            Logger.getLogger(Bill.class.getName()).log(Level.SEVERE, null, ex);
        }
   // DBConnect.disconnect();
    
    
    }
    
    
}
