
package portermanagementsystem;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JFrame;

public class Request_Accept_Screen extends javax.swing.JFrame {
    int userid;Timer timer=new Timer();int rid;int mintCount=0;
    public Request_Accept_Screen(int userid,int rid) {
        initComponents();
        setid(userid);
        name.setText(SystemUser.userName(userid));
        start();
        setrid(rid);
        name1.setText(SystemUser.userName(userid));
        back.setVisible(false);
    }
int arrivedCompleteSwitch=0;    
void setrid(int rid)
{
this.rid=rid;
}
int getrid()
{
return rid;
}
int getid()
  {
   return userid;
  }
  void setid(int userid)
  {
  this.userid=userid;
  }
   public String time()
        { DateFormat df = new SimpleDateFormat("HH:mm:ss");
       Date dateobj = new Date();
      return df.format(dateobj);}
public String getDate()
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	Date date = new Date();
	return dateFormat.format(date);
    }
    
  TimerTask task=new TimerTask()
  {
  public void run()
  {
  time.setText(time());
  date.setText(getDate());
  checkStatus();
  mintCount++;
  setButtons();
  System.out.println("nimtCount="+mintCount);
  }
  
  };
  public boolean checkMint()
  {
        return mintCount <= 120;
  }
  void setButtons()
  {
  if(!checkMint()||"Arrived".equals(Request.checkStatus(rid)))
  {
  
  transfer.setEnabled(false);
  }
  }
  public void  start()
  {
  
  timer.scheduleAtFixedRate(task, 1100, 1100);
  
  }
  public void decorate(int rid)
  {
  name.setText(Request.getName(rid));
  time1.setText(Request.getTime(rid));
  cost.setText(""+Request.getCost(rid));
  location.setText(Request.getLocation(rid));
  weight.setText(""+Request.getweight(rid));
  bags.setText(""+Request.getbag(rid));
  
  }
  public void checkStatus()
  {String s=Request.checkStatus(rid);
  status.setText(s);
  if(s.equals("Canceled"))
  back.setVisible(true);
  }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        time = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        name1 = new javax.swing.JLabel();
        back = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        date = new javax.swing.JLabel();
        jButton4 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        employeename1 = new javax.swing.JLabel();
        name = new javax.swing.JLabel();
        transfer = new javax.swing.JButton();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        cost = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        location = new javax.swing.JLabel();
        weight = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        bags = new javax.swing.JLabel();
        cancel = new javax.swing.JButton();
        kjlk = new javax.swing.JLabel();
        time1 = new javax.swing.JLabel();
        status = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        Arrived = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(40, 50, 80));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        time.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        time.setForeground(new java.awt.Color(254, 192, 0));
        time.setText("Time");
        jPanel1.add(time, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 490, 100, 30));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/portermanagementsystem/porterlogo.jpg"))); // NOI18N
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 160, 310, 220));

        jLabel2.setIcon(new javax.swing.ImageIcon("C:\\Users\\zaink\\Desktop\\p.png")); // NOI18N
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 20, 90, 90));

        name1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        name1.setForeground(new java.awt.Color(254, 192, 0));
        name1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        name1.setText("hghkhh");
        jPanel1.add(name1, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 120, 130, 30));

        back.setBackground(new java.awt.Color(255, 255, 255));
        back.setIcon(new javax.swing.ImageIcon("C:\\Users\\zaink\\Desktop\\backbutton2.png")); // NOI18N
        back.setBorder(null);
        back.setBorderPainted(false);
        back.setContentAreaFilled(false);
        back.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        back.setDefaultCapable(false);
        back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backActionPerformed(evt);
            }
        });
        jPanel1.add(back, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 60, 50, 50));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 360, 530));

        jPanel2.setBackground(new java.awt.Color(0, 102, 110));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        date.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        date.setForeground(new java.awt.Color(254, 192, 0));
        date.setText("Date");
        jPanel2.add(date, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 490, 80, 30));

        jButton4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton4.setForeground(new java.awt.Color(204, 102, 0));
        jButton4.setText("X");
        jButton4.setBorder(null);
        jButton4.setBorderPainted(false);
        jButton4.setContentAreaFilled(false);
        jButton4.setFocusPainted(false);
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton4, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 0, 30, 30));

        jButton6.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButton6.setForeground(new java.awt.Color(204, 102, 0));
        jButton6.setText("-");
        jButton6.setBorder(null);
        jButton6.setBorderPainted(false);
        jButton6.setContentAreaFilled(false);
        jButton6.setFocusPainted(false);
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton6, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 0, 30, 30));

        employeename1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        employeename1.setForeground(new java.awt.Color(254, 192, 0));
        employeename1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        employeename1.setText(" Request Portal");
        jPanel2.add(employeename1, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 70, 170, 30));

        name.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        name.setForeground(new java.awt.Color(254, 192, 0));
        name.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        name.setText("Cost");
        name.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(name, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 130, 90, 30));

        transfer.setText("Transfer Req.");
        transfer.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        transfer.setBorderPainted(false);
        transfer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                transferActionPerformed(evt);
            }
        });
        jPanel2.add(transfer, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 410, 90, 40));

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(254, 192, 0));
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setText("Name");
        jLabel13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 130, 90, 30));

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(254, 192, 0));
        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel14.setText("Cost");
        jLabel14.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 210, 90, 30));

        cost.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        cost.setForeground(new java.awt.Color(254, 192, 0));
        cost.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        cost.setText("Cost");
        cost.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(cost, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 210, 90, 30));

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(254, 192, 0));
        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel16.setText("Location");
        jLabel16.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 250, 90, 30));

        location.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        location.setForeground(new java.awt.Color(254, 192, 0));
        location.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        location.setText("Cost");
        location.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(location, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 250, 90, 30));

        weight.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        weight.setForeground(new java.awt.Color(254, 192, 0));
        weight.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        weight.setText("Cost");
        weight.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(weight, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 290, 90, 30));

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(254, 192, 0));
        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel19.setText("Weight");
        jLabel19.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 290, 90, 30));

        jLabel20.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(254, 192, 0));
        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel20.setText("Bags");
        jLabel20.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 330, 90, 30));

        bags.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bags.setForeground(new java.awt.Color(254, 192, 0));
        bags.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        bags.setText("Cost");
        bags.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(bags, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 330, 90, 30));

        cancel.setText("Cancel Request");
        cancel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        cancel.setBorderPainted(false);
        cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelActionPerformed(evt);
            }
        });
        jPanel2.add(cancel, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 460, 190, 40));

        kjlk.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        kjlk.setForeground(new java.awt.Color(254, 192, 0));
        kjlk.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        kjlk.setText("Time");
        kjlk.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(kjlk, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 170, 90, 30));

        time1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        time1.setForeground(new java.awt.Color(254, 192, 0));
        time1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        time1.setText("Cost");
        time1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(time1, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 170, 90, 30));

        status.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        status.setForeground(new java.awt.Color(254, 192, 0));
        status.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        status.setText("Cost");
        status.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(status, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 370, 90, 30));

        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(254, 192, 0));
        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("Status");
        jLabel21.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(jLabel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 370, 90, 30));

        Arrived.setText("Arrived");
        Arrived.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Arrived.setBorderPainted(false);
        Arrived.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ArrivedActionPerformed(evt);
            }
        });
        jPanel2.add(Arrived, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 410, 90, 40));

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 0, 390, 530));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        System.exit(0);       
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        this.setState(JFrame.ICONIFIED);

       
    }//GEN-LAST:event_jButton6ActionPerformed

    private void ArrivedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ArrivedActionPerformed
if(arrivedCompleteSwitch==0)
{     Request.changeStatus("Arrived", rid);
arrivedCompleteSwitch++;
Arrived.setText("Completed");
cancel.setEnabled(false);
transfer.setEnabled(false);
}
else
{
this.dispose();
Request.changeStatus("Completed", rid);
new Bill_Recieving_Screen(userid,rid).setVisible(true);
}

        // TODO add your handling code here:
    }//GEN-LAST:event_ArrivedActionPerformed

    private void backActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backActionPerformed

this.dispose();
new Request_screen_porter(userid).setVisible(true);
        // TODO add your handling code here:
    }//GEN-LAST:event_backActionPerformed

    private void cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelActionPerformed

Request.changeStatus("Canceled", rid);
PoterCancelRequest.insertPoterCancelRequest(rid, userid);
Wallet.createWallet(userid, -Rates.getRequestRate());
this.setVisible(false);
new Porter_Screen(userid).setVisible(true);
        // TODO add your handling code here:
    }//GEN-LAST:event_cancelActionPerformed

    private void transferActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_transferActionPerformed

Request.changeStatus("Transfered", rid);
PoterTransferRequest.insertPoterTransferRequest(rid, userid);
this.dispose();
new Request_screen_porter(userid).setVisible(true);
        // TODO add your handling code here:
    }//GEN-LAST:event_transferActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Request_Accept_Screen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Request_Accept_Screen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Request_Accept_Screen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Request_Accept_Screen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Arrived;
    private javax.swing.JButton back;
    private javax.swing.JLabel bags;
    private javax.swing.JButton cancel;
    private javax.swing.JLabel cost;
    private javax.swing.JLabel date;
    private javax.swing.JLabel employeename1;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel kjlk;
    private javax.swing.JLabel location;
    private javax.swing.JLabel name;
    private javax.swing.JLabel name1;
    private javax.swing.JLabel status;
    private javax.swing.JLabel time;
    private javax.swing.JLabel time1;
    private javax.swing.JButton transfer;
    private javax.swing.JLabel weight;
    // End of variables declaration//GEN-END:variables
}
