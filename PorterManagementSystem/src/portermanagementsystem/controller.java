
package portermanagementsystem;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class controller 
{
    private static int id;
    private static String status;
    
    public static void openConnection()
    {
    DBConnect2.getConnection();
        try {
            DBConnect2.getStatement().executeUpdate("Update controller set status='open' where id=1");
            
        } catch (SQLException ex) {
            Logger.getLogger(controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    DBConnect2.disconnect();
    }
    public static void closeConnection()
    {
    DBConnect2.getConnection();
        try {
            DBConnect2.getStatement().executeUpdate("Update controller set status='close' where id=1");
        } catch (SQLException ex) {
            Logger.getLogger(controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    DBConnect2.disconnect();
    }
    public static boolean checkConnection()
    {
        String s="";
    DBConnect2.getConnection();
        try {
            ResultSet r=DBConnect2.getStatement().executeQuery("select status from controller where id=1");
            while(r.next())
                s=r.getString("status");
        } catch (SQLException ex) {
            Logger.getLogger(controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    DBConnect2.disconnect();
        return s.equals("open");
    
    }
    
}
