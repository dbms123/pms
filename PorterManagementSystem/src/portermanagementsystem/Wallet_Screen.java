/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package portermanagementsystem;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;

/**
 *
 * @author zaink
 */
public class Wallet_Screen extends javax.swing.JFrame {
int userid;
    /**
     * Creates new form Manager_Screen
     */
      Timer timer=new Timer();
    public Wallet_Screen(int userid) {
        initComponents();
        setting.setComponentPopupMenu(popupmenu);
        start();
        setid(userid);
        name.setText(SystemUser.userName(userid));
        credit.setText(""+(Wallet.getCredit(userid)));
        
    }
  public String time()
        { DateFormat df = new SimpleDateFormat("HH:mm:ss");
       Date dateobj = new Date();
      return df.format(dateobj);}
public String getDate()
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	Date date = new Date();
	return dateFormat.format(date);
    }
  TimerTask task=new TimerTask()
  {
  public void run()
  {
  time.setText(time());
  date.setText(getDate());
  
  }
  
  };
  
  public void  start()
  {
  
  timer.scheduleAtFixedRate(task, 1000, 1000);
  
  }
  int getid()
  {
   return userid;
  }
  void setid(int userid)
  {
  this.userid=userid;
  }
 
            
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        popupmenu = new javax.swing.JPopupMenu();
        changepassword = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        changenumber = new javax.swing.JMenuItem();
        jPanel1 = new javax.swing.JPanel();
        time = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        name = new javax.swing.JLabel();
        back = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        date = new javax.swing.JLabel();
        jButton4 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        employeename1 = new javax.swing.JLabel();
        setting = new javax.swing.JButton();
        credit = new javax.swing.JLabel();
        kjlk1 = new javax.swing.JLabel();

        changepassword.setText("Change Password");
        popupmenu.add(changepassword);
        popupmenu.add(jSeparator1);

        changenumber.setText("Change phone Number");
        popupmenu.add(changenumber);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(40, 50, 80));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        time.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        time.setForeground(new java.awt.Color(254, 192, 0));
        time.setText("Time");
        jPanel1.add(time, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 489, 100, 30));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/portermanagementsystem/porterlogo.jpg"))); // NOI18N
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 160, 310, 220));

        jLabel2.setIcon(new javax.swing.ImageIcon("C:\\Users\\zaink\\Desktop\\p.png")); // NOI18N
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 20, 90, 90));

        name.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        name.setForeground(new java.awt.Color(254, 192, 0));
        name.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        name.setText("hghkhh");
        jPanel1.add(name, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 120, 130, 30));

        back.setBackground(new java.awt.Color(255, 255, 255));
        back.setIcon(new javax.swing.ImageIcon("C:\\Users\\zaink\\Desktop\\backbutton2.png")); // NOI18N
        back.setBorder(null);
        back.setBorderPainted(false);
        back.setContentAreaFilled(false);
        back.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        back.setDefaultCapable(false);
        back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backActionPerformed(evt);
            }
        });
        jPanel1.add(back, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 60, 50, 50));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 360, 530));

        jPanel2.setBackground(new java.awt.Color(0, 102, 110));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        date.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        date.setForeground(new java.awt.Color(254, 192, 0));
        date.setText("Date");
        jPanel2.add(date, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 480, 100, 30));

        jButton4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton4.setForeground(new java.awt.Color(204, 102, 0));
        jButton4.setText("X");
        jButton4.setBorder(null);
        jButton4.setBorderPainted(false);
        jButton4.setContentAreaFilled(false);
        jButton4.setFocusPainted(false);
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton4, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 0, 30, 30));

        jButton6.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButton6.setForeground(new java.awt.Color(204, 102, 0));
        jButton6.setText("-");
        jButton6.setBorder(null);
        jButton6.setBorderPainted(false);
        jButton6.setContentAreaFilled(false);
        jButton6.setFocusPainted(false);
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton6, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 0, 30, 30));

        employeename1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        employeename1.setForeground(new java.awt.Color(254, 192, 0));
        employeename1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        employeename1.setText("Wallet ");
        jPanel2.add(employeename1, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 70, 170, 30));

        setting.setBackground(new java.awt.Color(255, 255, 255));
        setting.setIcon(new javax.swing.ImageIcon(getClass().getResource("/portermanagementsystem/setting.png"))); // NOI18N
        setting.setBorder(null);
        setting.setBorderPainted(false);
        setting.setComponentPopupMenu(popupmenu);
        setting.setContentAreaFilled(false);
        setting.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setting.setDefaultCapable(false);
        setting.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                settingActionPerformed(evt);
            }
        });
        jPanel2.add(setting, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 60, 50, 50));

        credit.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        credit.setForeground(new java.awt.Color(254, 192, 0));
        credit.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        credit.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(credit, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 250, 100, 30));

        kjlk1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        kjlk1.setForeground(new java.awt.Color(254, 192, 0));
        kjlk1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        kjlk1.setText("Current Amount");
        kjlk1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(kjlk1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 250, 150, 30));

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 0, 390, 530));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void settingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_settingActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_settingActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        System.exit(0);        // TODO add your handling code here:
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        this.setState(JFrame.ICONIFIED);

        // TODO add your handling code here:
    }//GEN-LAST:event_jButton6ActionPerformed

    private void backActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backActionPerformed

        this.dispose();
        if(SystemUser.userType(UserLogin.getloginid(userid)).equals("Customer"))
        new Customer_Screen(userid).setVisible(true);
        else if(SystemUser.userType(UserLogin.getloginid(userid)).equals("Porter"))
            new Porter_Screen(userid).setVisible(true);
        // TODO add your handling code here:
    }//GEN-LAST:event_backActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Wallet_Screen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Wallet_Screen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Wallet_Screen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Wallet_Screen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
       
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton back;
    private javax.swing.JMenuItem changenumber;
    private javax.swing.JMenuItem changepassword;
    private javax.swing.JLabel credit;
    private javax.swing.JLabel date;
    private javax.swing.JLabel employeename1;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JLabel kjlk1;
    private javax.swing.JLabel name;
    private javax.swing.JPopupMenu popupmenu;
    private javax.swing.JButton setting;
    private javax.swing.JLabel time;
    // End of variables declaration//GEN-END:variables
}
