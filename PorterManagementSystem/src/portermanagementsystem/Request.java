/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package portermanagementsystem;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Asad
 */
public class Request {
    private static int RID=0;
    private static int CID=0;
    private static int PID=0;
    private static int RLocation=0;
    private static int Weight=0;
    private static int RequestCost=0;
    private static String Status="";
    private static int noOfRecords=0;

public static void refresh()
{
    RID=0;
    CID=0;
    PID=0;
    RLocation=0;
    Weight=0;
    Status="";
    RequestCost=0;
    noOfRecords=0;
}
static void setnoOfRecords()
{

//DBConnect.getConnection();
    try {
        ResultSet resultSet=DBConnect.getStatement().executeQuery("SELECT * FROM REQUEST WHERE RID = (SELECT MAX(RID) FROM Request)");
        while(resultSet.next())
             noOfRecords=resultSet.getInt(1);
        } catch (SQLException ex) 
        {Logger.getLogger(SystemUser.class.getName()).log(Level.SEVERE, null, ex);}
//DBConnect.disconnect();


}
static void setnoOfRecordsForAccepted()
{   noOfRecords=0;
   
    
//DBConnect.getConnection();
    try {
        ResultSet resultSet=DBConnect.getStatement().executeQuery("SELECT * FROM REQUEST WHERE status='Not Accepted'");
        while(resultSet.next())
        {noOfRecords++;
        System.out.print(noOfRecords);
        }
        
        } catch (SQLException ex) 
        {Logger.getLogger(SystemUser.class.getName()).log(Level.SEVERE, null, ex);}
//DBConnect.disconnect();


}
static void setnoOfRecordsForTransfered()
{   noOfRecords=0;
   
    
//DBConnect.getConnection();
    try {
        ResultSet resultSet=DBConnect.getStatement().executeQuery("SELECT * FROM REQUEST WHERE status='Transfered'");
        while(resultSet.next())
        {
            noOfRecords++;
        
        }
        
        } catch (SQLException ex) 
        {Logger.getLogger(SystemUser.class.getName()).log(Level.SEVERE, null, ex);}
//DBConnect.disconnect();


}
static void setnoOfRecordsForPorterAttempts(int pid)
{   noOfRecords=0;

//DBConnect.getConnection();
    try {
        ResultSet resultSet=DBConnect.getStatement().executeQuery("SELECT * FROM REQUEST WHERE pid="+pid);
        while(resultSet.next())
        {noOfRecords++;
        System.out.print(noOfRecords);
        }
        
        } catch (SQLException ex) 
        {Logger.getLogger(SystemUser.class.getName()).log(Level.SEVERE, null, ex);}
//DBConnect.disconnect();



}
static void setnoOfRecordsForCustomerAttempts(int cid)
{   noOfRecords=0;

//DBConnect.getConnection();
    try {
        ResultSet resultSet=DBConnect.getStatement().executeQuery("SELECT * FROM REQUEST WHERE cid="+cid);
        while(resultSet.next())
        {noOfRecords++;
        System.out.print(noOfRecords);
        }
        
        } catch (SQLException ex) 
        {Logger.getLogger(SystemUser.class.getName()).log(Level.SEVERE, null, ex);}
//DBConnect.disconnect();



}

static int getnoOfRecordsForTransfered()
{
setnoOfRecordsForTransfered();
return noOfRecords;
}

static int getnoOfRecordsForCustomerAttempts(int cid )
{
setnoOfRecordsForCustomerAttempts(cid);
return noOfRecords;
}
static int getnoOfRecordsForPorterAttempts(int pid )
{
setnoOfRecordsForPorterAttempts(pid);
return noOfRecords;
}
static int getnoOfRecordsForAccepted()
{
setnoOfRecordsForAccepted();
return noOfRecords;
}
static int getnoOfRecords()
{
setnoOfRecords();
return noOfRecords;
}
static String getDate()
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	Date date = new Date();
	return dateFormat.format(date);
    }
public static int makeRequest(int cid, String rlocation, int weight, int requestcost,int bag)
{   RID=getnoOfRecords()+1;
System.out.println("RID="+RID);
System.out.println("CID="+cid);
System.out.println("Date="+getDate());
System.out.println("Location="+rlocation);
System.out.println("Cost="+requestcost);
System.out.println("time="+time());
System.out.println("Bag="+bag);
    //DBConnect.getConnection();
    try {   if(weight==0)
            DBConnect.getStatement().executeQuery("Insert into REQUEST (RID,CID,DATES,RLOCATION,REQUESTCOST,TIME,STATUS,BAG) values("+RID+","+cid+",'"+getDate()+"','"+rlocation+"',"+requestcost+",'"+time()+"','Not Accepted',"+bag+")");
            else
            DBConnect.getStatement().executeQuery("Insert into REQUEST (RID,CID,DATES,RLOCATION,WEIGHT,REQUESTCOST,TIME,STATUS) values("+RID+","+cid+",'"+getDate()+"','"+rlocation+"',"+weight+","+requestcost+",'"+time()+"','Not Accepted')");
                 
    } 
    catch (SQLException ex) {
            Logger.getLogger(UserContact.class.getName()).log(Level.SEVERE, null, ex);
        }
    int a=RID;
    refresh();
  //  DBConnect.disconnect();
    return a;
}
public static String time()
        { DateFormat df = new SimpleDateFormat("HH:mm:ss");
       Date dateobj = new Date();
      return df.format(dateobj);
        }

public static int changeStatus(String status,int rid)
{int a=-1;
//DBConnect.getConnection();
        try {
           a= DBConnect.getStatement().executeUpdate("update request set status='"+status+"' where rid="+rid);
        } catch (SQLException ex) {
            Logger.getLogger(Request.class.getName()).log(Level.SEVERE, null, ex);
        }
        //DBConnect.disconnect();
return a;
}
public static String checkStatus(int rid)
{String a="";
//DBConnect.getConnection();
        try {
           ResultSet r= DBConnect.getStatement().executeQuery("select status from request where rid="+rid);
           while(r.next())
               a=r.getString("STATUS");
        } catch (SQLException ex) {
            Logger.getLogger(Request.class.getName()).log(Level.SEVERE, null, ex);
        }
       // DBConnect.disconnect();
return a;
}
public static String[][]getunacceptedrequests()
{

String a[][];
a=new String[getnoOfRecordsForAccepted()+1][6];int i=0;
//DBConnect.getConnection();
        try {
            ResultSet r=DBConnect.getStatement().executeQuery("select request.rid,systemuser.firstname,request.rlocation,request.time,request.weight,request.bag from request,systemuser where request.cid=systemuser.userid and request.status='Not Accepted'");
            while(r.next())
            {
            a[i][0]=r.getInt("rid")+"";
            a[i][1]=r.getString("FIRSTNAME");
            a[i][2]=r.getString("RLOCATION");
            a[i][3]=r.getString("time");
            a[i][4]=r.getInt("weight")+"";
            a[i][5]=r.getInt("bag")+"";
            
            i++;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Request.class.getName()).log(Level.SEVERE, null, ex);
//DBConnect.disconnect();
        }
return a;
}
public static String[][]getTransferedrequests()
{

String a[][];
a=new String[getnoOfRecordsForTransfered()+1][6];int i=0;
//DBConnect.getConnection();
        try {
            ResultSet r=DBConnect.getStatement().executeQuery("select request.rid,systemuser.firstname,request.rlocation,request.time,request.weight,request.bag from request,systemuser where request.cid=systemuser.userid and request.status='Transfered'");
            while(r.next())
            {
            a[i][0]=r.getInt("rid")+"";
            a[i][1]=r.getString("FIRSTNAME");
            a[i][2]=r.getString("RLOCATION");
            a[i][3]=r.getString("time");
            a[i][4]=r.getString("weight");
            a[i][5]=r.getString("bag");
            
            i++;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Request.class.getName()).log(Level.SEVERE, null, ex);
//DBConnect.disconnect();
        }
return a;
}
public static String[][]getPreviousAttemptsForPorter(int pid)
{

String a[][];
a=new String[getnoOfRecordsForPorterAttempts(pid)+1][7];int i=0;
//DBConnect.getConnection();
        try {
            ResultSet r=DBConnect.getStatement().executeQuery("select request.rid,systemuser.firstname,request.rlocation,request.time,request.weight,request.bag,request.dates from request,systemuser where request.cid=systemuser.userid and request.pid="+pid);
            while(r.next())
            {
            a[i][0]=r.getInt("rid")+"";
            a[i][1]=r.getString("FIRSTNAME");
            a[i][2]=r.getString("RLOCATION");
            a[i][3]=r.getString("time");
            a[i][4]=r.getString("weight");
            a[i][5]=r.getString("bag");
            a[i][6]=r.getString("dates");
            i++;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Request.class.getName()).log(Level.SEVERE, null, ex);
//DBConnect.disconnect();
        }
return a;
}
public static String[][]getPreviousAttemptsForCustomer(int cid)
{

String a[][];
a=new String[getnoOfRecordsForCustomerAttempts(cid)+1][7];int i=0;
//DBConnect.getConnection();
        try {
            ResultSet r=DBConnect.getStatement().executeQuery("select request.rid,systemuser.firstname,request.rlocation,request.time,request.weight,request.bag,request.dates from request,systemuser where request.pid=systemuser.userid and request.cid="+cid);
            while(r.next())
            {
            a[i][0]=r.getInt("rid")+"";
            a[i][1]=r.getString("FIRSTNAME");
            a[i][2]=r.getString("RLOCATION");
            a[i][3]=r.getString("time");
            a[i][4]=r.getString("weight");
            a[i][5]=r.getString("bag");
            a[i][6]=r.getString("dates");
            i++;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Request.class.getName()).log(Level.SEVERE, null, ex);
//DBConnect.disconnect();
        }
return a;
}
public static int getnoofunacceptedrequest()
{
int a=0;
//DBConnect.getConnection();
        try {
            ResultSet r=DBConnect.getStatement().executeQuery("select * from request where status='Not Accepted'");
            while(r.next())
            {
            a++;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Request.class.getName()).log(Level.SEVERE, null, ex);
        }
//DBConnect.disconnect();
        return a;
}
public static void acceptRequest(int rid,int pid)
{

    //DBConnect.getConnection();
        try {
           DBConnect.getStatement().executeUpdate("update request set status='Accepted',pid="+pid+"where rid="+rid);
        } catch (SQLException ex) {
            Logger.getLogger(Request.class.getName()).log(Level.SEVERE, null, ex);
        }
        //DBConnect.disconnect();

}
public static boolean checkRequestID(int rid)
{ int a=0;
       // DBConnect.getConnection();
        try {
          a =DBConnect.getStatement().executeUpdate("select rid from request where rid="+rid);
           
        } catch (SQLException ex) {
            Logger.getLogger(Request.class.getName()).log(Level.SEVERE, null, ex);
        }
        //DBConnect.disconnect();
        return a != 0;

}
public static int getCID(int rid)
{
int a=0;
//DBConnect.getConnection();
        try {
            ResultSet r=DBConnect.getStatement().executeQuery("select cid from request where rid="+rid);
            while(r.next())
            {
            a=r.getInt("CID");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Wallet.class.getName()).log(Level.SEVERE, null, ex);
        }
 //DBConnect.disconnect();
        return a;
}
public static int getPID(int rid)
{
int a=0;
//DBConnect.getConnection();
        try {
            ResultSet r=DBConnect.getStatement().executeQuery("select pid from request where rid="+rid);
            while(r.next())
            {
            a=r.getInt("PID");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Wallet.class.getName()).log(Level.SEVERE, null, ex);
        }
 //DBConnect.disconnect();
        return a;
}
public static String getName(int rid)
{
        return SystemUser.userName(getCID(rid));
}
public static int getCost(int rid)
{
int a=0;
//DBConnect.getConnection();
        try {
            ResultSet r=DBConnect.getStatement().executeQuery("select requestcost from request where rid="+rid);
            while(r.next())
            {
            a=r.getInt("requestcost");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Wallet.class.getName()).log(Level.SEVERE, null, ex);
        }
 //DBConnect.disconnect();
        return a;
}
public static String getLocation(int rid)
{
String a="";
//DBConnect.getConnection();
        try {
            ResultSet r=DBConnect.getStatement().executeQuery("select rlocation from request where rid="+rid);
            while(r.next())
            {
            a=r.getString("rlocation");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Wallet.class.getName()).log(Level.SEVERE, null, ex);
        }
 //DBConnect.disconnect();
        return a;
}
public static String getTime(int rid)
{
String a="";
//DBConnect.getConnection();
        try {
            ResultSet r=DBConnect.getStatement().executeQuery("select time from request where rid="+rid);
            while(r.next())
            {
            a=r.getString("time");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Wallet.class.getName()).log(Level.SEVERE, null, ex);
        }
 //DBConnect.disconnect();
        return a;
}
public static int getweight(int rid)
{
int a=0;
//DBConnect.getConnection();
        try {
            ResultSet r=DBConnect.getStatement().executeQuery("select weight from request where rid="+rid);
            while(r.next())
            {
            a=r.getInt("weight");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Wallet.class.getName()).log(Level.SEVERE, null, ex);
        }
 //DBConnect.disconnect();
        return a;
}
public static int getbag(int rid)
{
int a=0;
//DBConnect.getConnection();
        try {
            ResultSet r=DBConnect.getStatement().executeQuery("select bag from request where rid="+rid);
            while(r.next())
            {
            a=r.getInt("bag");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Wallet.class.getName()).log(Level.SEVERE, null, ex);
        }
 //DBConnect.disconnect();
        return a;

}
}

