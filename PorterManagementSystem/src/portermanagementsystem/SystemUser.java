
package portermanagementsystem;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class SystemUser {
    private static int UserID=0;
    private static String FirstName="";
    private static String LastName="";
    private static int Age=0;
    private static String Address="";
    private static String UserType="";
    private static int noOfRecords;
public static void refresh()
{
    UserID=0;
    FirstName="";
    LastName="";
    Age=0;
    Address="";
    UserType="";
    
}
static void setnoOfRecords()
{
//DBConnect.getConnection();
    try {
        ResultSet resultSet=DBConnect.getStatement().executeQuery("SELECT * FROM SYSTEMUSER WHERE USERID = (SELECT MAX(USERID) FROM SYSTEMUSER)");
        while(resultSet.next())
             noOfRecords=resultSet.getInt(1);
        } catch (SQLException ex) 
        {Logger.getLogger(SystemUser.class.getName()).log(Level.SEVERE, null, ex);}
//DBConnect.disconnect();

}
static int getnoOfRecords()
{
    setnoOfRecords();
return noOfRecords;
}
public static boolean add_user(String firstname, String lastname, int age,String address, String usertype,String loginid, String password, String phonenumber)
{
UserID=getnoOfRecords()+1;
//DBConnect.getConnection();
    try {
     
        DBConnect.getStatement().executeUpdate("Insert into SYSTEMUSER values("+UserID+",'"+firstname+"',"+age+",'"+usertype+"','"+address+"','"+lastname+"')");
        
        } catch (SQLException ex) 
        {Logger.getLogger(SystemUser.class.getName()).log(Level.SEVERE, null, ex);}
    //DBConnect.disconnect(); 
    UserContact.add_user_contact(UserID, phonenumber);
    UserLogin.add_user_login(UserID, loginid, password);
    refresh();
    return true;
}
public static String userType(String loginid)
{

//DBConnect.getConnection();
        try {
            ResultSet r=DBConnect.getStatement().executeQuery("select usertype from systemuser where userid=(select distinct userid from userlogin where loginid='"+loginid+"')");
               while(r.next())
                   UserType=r.getString("USERTYPE");
        } catch (SQLException ex) {
            Logger.getLogger(SystemUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        String s=UserType;
        refresh();
        //DBConnect.disconnect();
        return s;
}
public static String userName(int userid)
{

//DBConnect.getConnection();
        try {
            ResultSet r=DBConnect.getStatement().executeQuery("select FIRSTNAME from systemuser where userid="+userid+"");
               while(r.next())
                   UserType=r.getString("firstname");
        } catch (SQLException ex) {
            Logger.getLogger(SystemUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        String s=UserType;
        refresh();
       // DBConnect.disconnect();
        return s;
}
public static int getUserID(String loginid)
{

//DBConnect.getConnection();
        try {
            ResultSet r=DBConnect.getStatement().executeQuery("select userid from userlogin where loginid='"+loginid+"'");
               while(r.next())
                   UserID=r.getInt("USERID");
        } catch (SQLException ex) {
            Logger.getLogger(SystemUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        int s=UserID;
        refresh();
       // DBConnect.disconnect();
        return s;
}
}
