
package portermanagementsystem;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Rates 
{
    private static int RateID;
    private static int Bag;
    private static int Weight;
    private static int RequestRate;
    private static int noOfRecords;
    
static String getDate()
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	Date date = new Date();
	return dateFormat.format(date);
    }    
static void setnoOfRecords()
{
//DBConnect.getConnection();
    try {
        ResultSet resultSet=DBConnect.getStatement().executeQuery("SELECT RATEID FROM RATES WHERE RATEID = (SELECT MAX(RATEID) FROM RATES)");
        while(resultSet.next())
        {    noOfRecords=resultSet.getInt("RATEID");}
        } catch (SQLException ex) 
        {Logger.getLogger(SystemUser.class.getName()).log(Level.SEVERE, null, ex);}
//DBConnect.disconnect();

}
static int getnoOfRecords()
{
    setnoOfRecords();
return noOfRecords;
}
public static void refresh()
{
RateID=0;
Bag=0;
Weight=0;
RequestRate=0;
noOfRecords=0;
}
public static boolean addRates(int bags,int weight,int requestrate)        
{
    deactivateRates();
    RateID=getnoOfRecords()+1;
    //DBConnect.getConnection();
        try {
           DBConnect.getStatement().executeQuery("Insert into RATES values("+RateID+","+bags+","+weight+","+requestrate+",'"+getDate()+"','Active')");
        } catch (SQLException ex) {
            Logger.getLogger(Rates.class.getName()).log(Level.SEVERE, null, ex);
        }
       // DBConnect.disconnect();
        
        refresh();
return true;
}
    public static int deactivateRates()
    {
    //DBConnect.getConnection();
    int r=-1;
    try {
        r=DBConnect.getStatement().executeUpdate("UPDATE RATES set STATUS='Deactivated' where STATUS='Active'");
    } catch (SQLException ex) {
        Logger.getLogger(UserLogin.class.getName()).log(Level.SEVERE, null, ex);
    }
    //DBConnect.disconnect();
    refresh();
    return r;
    }
    public static int getBag()
    {
   // DBConnect.getConnection();
        try {
            ResultSet r=DBConnect.getStatement().executeQuery("SELECT BAG FROM RATES WHERE STATUS='Active'");
            while(r.next())
            {  Bag=r.getInt("BAG");}
        } catch (SQLException ex) {
            Logger.getLogger(Rates.class.getName()).log(Level.SEVERE, null, ex);
        }
    int a=Bag;
    refresh();
   // DBConnect.disconnect();
    return a;
    }
    public static int getWeight()
    {
   // DBConnect.getConnection();
        try {
            ResultSet r=DBConnect.getStatement().executeQuery("SELECT WEIGHT FROM RATES WHERE STATUS='Active'");
            while(r.next())
            {    Weight=r.getInt("WEIGHT");}
        } catch (SQLException ex) {
            Logger.getLogger(Rates.class.getName()).log(Level.SEVERE, null, ex);
        }
    int a=Weight;
    refresh();
    //DBConnect.disconnect();
    return a;
    }
    public static int getRequestRate()
    {
        
   // DBConnect.getConnection();
        try {
            ResultSet r=DBConnect.getStatement().executeQuery("SELECT REQUESTRATE FROM RATES WHERE STATUS='Active'");
            while(r.next())
            {     RequestRate=r.getInt("REQUESTRATE");}
        } catch (SQLException ex) {
            Logger.getLogger(Rates.class.getName()).log(Level.SEVERE, null, ex);
        }
    int a=RequestRate;
    refresh();
  //  DBConnect.disconnect();
    return a;
    }
}
