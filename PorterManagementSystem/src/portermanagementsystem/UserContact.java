
package portermanagementsystem;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Asad
 */
public class UserContact {
    private static int UserID;
    private static String PhoneNumber;
    private static String NumberStatus;
    private static ResultSet resultset;
    public static void refresh()
    {
        UserID=0;
        PhoneNumber="";
        NumberStatus="";
    }
        static String getDate()
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	Date date = new Date();
	return dateFormat.format(date);
    }
public static boolean check_phonenumber(String phonenumber)
{
    boolean var=false;
    try {
    //DBConnect.getConnection();
    ResultSet r=DBConnect.getStatement().executeQuery("Select PHONENUMBER from USERCONTACT where PHONENUMBER='"+phonenumber+"'");
    while(r.next())
    {
    PhoneNumber=r.getString("PHONENUMBER");
    if(PhoneNumber== null ? phonenumber== null : phonenumber.equals(PhoneNumber))
    var=true;
    
    }
//DBConnect.disconnect();
    } 
catch (SQLException ex)
    {
    Logger.getLogger(PorterManagementSystem.class.getName()).log(Level.SEVERE, null, ex);
    }
    refresh();
    return var;
}
    public static boolean add_user_contact(int userid, String phonenumber)
            {
               // DBConnect.getConnection();
        try {
            DBConnect.getStatement().executeUpdate("insert into USERCONTACT values("+userid+",'"+phonenumber+"','Active','"+getDate()+"')");
        } catch (SQLException ex) {
            Logger.getLogger(UserContact.class.getName()).log(Level.SEVERE, null, ex);
        }
        refresh();
       // DBConnect.disconnect();
        return true;
             
            }
    public static int getID(String phonenumber)
    {
    //DBConnect.getConnection();
        try {
            resultset=DBConnect.getStatement().executeQuery("Select USERID from USERCONTACT where PHONENUMBER='"+phonenumber+"'");
            while(resultset.next())
                UserID=resultset.getInt(1);
        } catch (SQLException ex) {
            Logger.getLogger(UserContact.class.getName()).log(Level.SEVERE, null, ex);
            
        }
       int s=UserID;
       refresh();
       //DBConnect.disconnect();
       return s;
    }
    public static int deactivatePhonenumber(int userid)
    {
   // DBConnect.getConnection();
    int r=-1;
    try {
        r=DBConnect.getStatement().executeUpdate("UPDATE USERCONTACT set NUMBERSTATUS='Deactivated' where USERID="+userid);
    } catch (SQLException ex) {
        Logger.getLogger(UserLogin.class.getName()).log(Level.SEVERE, null, ex);
    }
   // DBConnect.disconnect();
    return r;
    }
}
