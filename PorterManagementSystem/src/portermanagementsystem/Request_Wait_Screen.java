/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package portermanagementsystem;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JFrame;

/**
 *
 * @author zaink
 */
public class Request_Wait_Screen extends javax.swing.JFrame {
int userid; Timer timer=new Timer();
int rid;int mintCount=0;
    public Request_Wait_Screen(int userid,int rid) {
        initComponents();
        
         start();
         setid(userid);
         setrid(rid);
        name.setText(SystemUser.userName(userid));
    }
void setrid(int rid)
{
this.rid=rid;
}
int getrid()
{
return rid;
}
  public boolean checkMint()
  {
        return mintCount <= 120;
  }
  void setButtons()
  {
  if(!checkMint())
  {
  cancel.setEnabled(false);
  
  }
  }
void decorate(boolean bweight,int weight,int bag,int cost,int location)
{
if(bweight)
{ this.bweight.setSelected(true);
  this.weight.setSelectedIndex(weight);
}
else
{ this.bbag.setSelected(true);
  this.bag.setSelectedIndex(bag);
}
this.cost.setText(""+cost);
this.location.setSelectedIndex(location);

/*this.bweight.setEnabled(false);
this.bbag.setEnabled(false);
this.location.setEnabled(false);
this.cost.setEnabled(false);*/


}
void checkStatus()
{
status.setText(Request.checkStatus(rid));
if(status.getText().equals("Arrived"))
   cancel.setEnabled(false);
else if(status.getText().equals("Completed"))
{
this.dispose();

new Bill_Screen(userid,rid).setVisible(true);
timer.cancel();
}
}

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        popupmenu = new javax.swing.JPopupMenu();
        changepassword = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        changenumber = new javax.swing.JMenuItem();
        jPanel1 = new javax.swing.JPanel();
        time = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        name = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        date = new javax.swing.JLabel();
        jButton4 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        employeename1 = new javax.swing.JLabel();
        bweight = new javax.swing.JRadioButton();
        bbag = new javax.swing.JRadioButton();
        bag = new javax.swing.JComboBox<>();
        location = new javax.swing.JComboBox<>();
        weight = new javax.swing.JComboBox<>();
        porter = new javax.swing.JLabel();
        cost = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        status = new javax.swing.JLabel();
        cancel = new javax.swing.JButton();

        changepassword.setText("Change Password");
        popupmenu.add(changepassword);
        popupmenu.add(jSeparator1);

        changenumber.setText("Change phone Number");
        popupmenu.add(changenumber);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(40, 50, 80));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        time.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        time.setForeground(new java.awt.Color(254, 192, 0));
        time.setText("Time");
        jPanel1.add(time, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 480, 100, 30));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/portermanagementsystem/porterlogo.jpg"))); // NOI18N
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 160, 310, 220));

        jLabel2.setIcon(new javax.swing.ImageIcon("C:\\Users\\zaink\\Desktop\\p.png")); // NOI18N
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 20, 90, 90));

        name.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        name.setForeground(new java.awt.Color(254, 192, 0));
        name.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        name.setText("hghkhh");
        jPanel1.add(name, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 120, 130, 30));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 360, 530));

        jPanel2.setBackground(new java.awt.Color(0, 102, 110));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        date.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        date.setForeground(new java.awt.Color(254, 192, 0));
        date.setText("Date");
        jPanel2.add(date, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 470, 80, 30));

        jButton4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton4.setForeground(new java.awt.Color(204, 102, 0));
        jButton4.setText("X");
        jButton4.setBorder(null);
        jButton4.setBorderPainted(false);
        jButton4.setContentAreaFilled(false);
        jButton4.setFocusPainted(false);
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton4, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 0, 30, 30));

        jButton6.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButton6.setForeground(new java.awt.Color(204, 102, 0));
        jButton6.setText("-");
        jButton6.setBorder(null);
        jButton6.setBorderPainted(false);
        jButton6.setContentAreaFilled(false);
        jButton6.setFocusPainted(false);
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton6, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 0, 30, 30));

        employeename1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        employeename1.setForeground(new java.awt.Color(254, 192, 0));
        employeename1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        employeename1.setText(" Request Portal");
        jPanel2.add(employeename1, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 70, 170, 30));

        buttonGroup1.add(bweight);
        bweight.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bweight.setForeground(new java.awt.Color(254, 192, 0));
        bweight.setText("By Weight");
        bweight.setBorder(null);
        bweight.setContentAreaFilled(false);
        bweight.setEnabled(false);
        jPanel2.add(bweight, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 150, 80, -1));

        buttonGroup1.add(bbag);
        bbag.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bbag.setForeground(new java.awt.Color(254, 192, 0));
        bbag.setText("By Bag No.");
        bbag.setBorder(null);
        bbag.setContentAreaFilled(false);
        bbag.setEnabled(false);
        bbag.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bbagActionPerformed(evt);
            }
        });
        jPanel2.add(bbag, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 150, 90, -1));

        bag.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bag.setForeground(new java.awt.Color(254, 192, 0));
        bag.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", "61", "62", "63", "64", "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "80", "81", "82", "83", "84", "85", "86", "87", "88", "89", "90", "91", "92", "93", "94", "95", "96", "97", "98", "99", "100" }));
        bag.setBorder(null);
        bag.setEnabled(false);
        bag.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bagActionPerformed(evt);
            }
        });
        jPanel2.add(bag, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 200, 90, 30));

        location.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        location.setForeground(new java.awt.Color(254, 192, 0));
        location.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Location", "Location 1", "Location 2", "Location 3", "Current Location" }));
        location.setBorder(null);
        location.setEnabled(false);
        jPanel2.add(location, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 260, 170, 30));

        weight.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        weight.setForeground(new java.awt.Color(254, 192, 0));
        weight.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", "61", "62", "63", "64", "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "80", "81", "82", "83", "84", "85", "86", "87", "88", "89", "90", "91", "92", "93", "94", "95", "96", "97", "98", "99", "100" }));
        weight.setBorder(null);
        weight.setEnabled(false);
        jPanel2.add(weight, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 200, 80, 30));

        porter.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        porter.setForeground(new java.awt.Color(254, 192, 0));
        porter.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jPanel2.add(porter, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 420, 90, 30));

        cost.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        cost.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        cost.setEnabled(false);
        cost.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                costActionPerformed(evt);
            }
        });
        jPanel2.add(cost, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 320, 80, 30));

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(254, 192, 0));
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("Cost");
        jLabel8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 240, 240), 2));
        jPanel2.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 320, 90, 30));

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(254, 192, 0));
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("Porter Name");
        jPanel2.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 420, 90, 30));

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(254, 192, 0));
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setText("Status");
        jPanel2.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 380, 90, 30));

        status.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        status.setForeground(new java.awt.Color(254, 192, 0));
        status.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        status.setText("Not Accepted");
        jPanel2.add(status, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 380, 90, 30));

        cancel.setText("Cancel Request");
        cancel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        cancel.setBorderPainted(false);
        cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelActionPerformed(evt);
            }
        });
        jPanel2.add(cancel, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 460, 170, 40));

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 0, 390, 530));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
  public String time()
        { DateFormat df = new SimpleDateFormat("HH:mm:ss");
       Date dateobj = new Date();
      return df.format(dateobj);}
public String getDate()
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	Date date = new Date();
	return dateFormat.format(date);
    }
  TimerTask task=new TimerTask()
  {
  public void run()
  {
  time.setText(time());
  date.setText(getDate());
  checkStatus();
  mintCount++;
  setButtons();
  }
  
  };
  
  public void  start()
  {
  
  timer.scheduleAtFixedRate(task, 1000, 1000);
  
  }
  int getid()
  {
   return userid;
  }
  void setid(int userid)
  {
  this.userid=userid;
  }
 
    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        System.exit(0);        // TODO add your handling code here:
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        this.setState(JFrame.ICONIFIED);

        // TODO add your handling code here:
    }//GEN-LAST:event_jButton6ActionPerformed

    private void bbagActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bbagActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_bbagActionPerformed

    private void bagActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bagActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_bagActionPerformed

    private void costActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_costActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_costActionPerformed

    private void cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelActionPerformed

Request.changeStatus("Canceled", rid);
if((mintCount>120&&mintCount<300)||status.getText().equals("Accepted"))
    Wallet.createWallet(userid, -Rates.getRequestRate());
CustomerCancelRequest.insertCustomerCancelRequest(rid, userid);
this.dispose();
new Customer_Screen(userid).setVisible(true);
     
    }//GEN-LAST:event_cancelActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Request_Wait_Screen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Request_Wait_Screen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Request_Wait_Screen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Request_Wait_Screen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> bag;
    private javax.swing.JRadioButton bbag;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JRadioButton bweight;
    private javax.swing.JButton cancel;
    private javax.swing.JMenuItem changenumber;
    private javax.swing.JMenuItem changepassword;
    private javax.swing.JTextField cost;
    private javax.swing.JLabel date;
    private javax.swing.JLabel employeename1;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JComboBox<String> location;
    private javax.swing.JLabel name;
    private javax.swing.JPopupMenu popupmenu;
    private javax.swing.JLabel porter;
    private javax.swing.JLabel status;
    private javax.swing.JLabel time;
    private javax.swing.JComboBox<String> weight;
    // End of variables declaration//GEN-END:variables
}
