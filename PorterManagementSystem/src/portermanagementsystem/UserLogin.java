
package portermanagementsystem;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import static portermanagementsystem.UserContact.refresh;


public class UserLogin 
{
static private int UserID=0;
static private String LoginID="";
static private String Password="";
static private String Date="";
static void refresh()
{
UserID=0;
LoginID="";
Password="";
Date="";
}
public static boolean login(String loginid,String password)
{
    boolean var=false;
try {
    //DBConnect.getConnection();
    ResultSet r=DBConnect.getStatement().executeQuery("Select PASSWORD from USERLOGIN where LOGINID='"+loginid+"'");
    while(r.next())
    {
    Password=r.getString("PASSWORD");
    if(Password == null ? password == null : Password.equals(password))
        var=true;
    }
    } 
catch (SQLException ex)
    {
    Logger.getLogger(PorterManagementSystem.class.getName()).log(Level.SEVERE, null, ex);
    }
//DBConnect.disconnect();
refresh();
return var;
}
public static boolean check_loginid(String loginid)
{
    boolean var=false;
    try {
   // DBConnect.getConnection();
    ResultSet r=DBConnect.getStatement().executeQuery("Select LOGINID from USERLOGIN where LOGINID='"+loginid+"'");
    while(r.next())
    {
    LoginID=r.getString("LOGINID");
    if(LoginID== null ? loginid== null : loginid.equals(LoginID))
        var=true;
    }
//DBConnect.disconnect();
    } 
catch (SQLException ex)
    {
    Logger.getLogger(PorterManagementSystem.class.getName()).log(Level.SEVERE, null, ex);
    }
    refresh();
    return var;
}
public static boolean add_user_login(int userid, String loginid, String password)
{
    //DBConnect.getConnection();
    try {
        DBConnect.getStatement().executeUpdate("Insert into USERLOGIN values ("+userid+",'"+password+"','"+getDate()+"','"+loginid+"','Active')");
    } catch (SQLException ex) {
        Logger.getLogger(UserLogin.class.getName()).log(Level.SEVERE, null, ex);
    }
  //  DBConnect.disconnect();
    refresh();
    return true;
}
public static String getloginid(int userid)
{
    String a="";
    try {
       ResultSet r =DBConnect.getStatement().executeQuery("Select loginid from UserLogin where userid="+userid);
        while(r.next())
        {
            a=r.getString("loginid");
        }
    } catch (SQLException ex) {
        Logger.getLogger(UserLogin.class.getName()).log(Level.SEVERE, null, ex);
    }
    return a;
}
    static String getDate()
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	Date date = new Date();
	return dateFormat.format(date);
    }
    
        public static int getID(String loginid)
    {
    //DBConnect.getConnection();
        try {
            ResultSet resultset=DBConnect.getStatement().executeQuery("Select USERID from USERLOGIN where LOGINID='"+loginid+"' and PASSWORDSTATUS='Active'");
            while(resultset.next())
                UserID=resultset.getInt(1);
        } catch (SQLException ex) {
            Logger.getLogger(UserContact.class.getName()).log(Level.SEVERE, null, ex);
            
        }
       int s=UserID;
       refresh();
      // DBConnect.disconnect();
       return s;
    }
    public static int deactivatePassword(int userid)
    {
   // DBConnect.getConnection();
    int r=-1;
    try {
        r=DBConnect.getStatement().executeUpdate("UPDATE USERLOGIN set PASSWORDSTATUS='Deactivated' where USERID="+userid);
    } catch (SQLException ex) {
        Logger.getLogger(UserLogin.class.getName()).log(Level.SEVERE, null, ex);
    }
   // DBConnect.disconnect();
    return r;
    }
    
}
